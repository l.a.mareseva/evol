### Result
##1
Первым этапом выполнения работы была попытка добиться искомого результата - 10 - на основе уже имеющегося кода, только за счет подбора параметров. Сначала была попытка определить при помощи перебора какие же параметры влияют больше всего на результат. Наиболее сильно влияло число популяции, чем было оно больше, тем больше станоился результат относительно запусков с такими же значениями для мутации и кроссовера. Поэтому было рещено взять значения mut_pro = 0.6,  cross_pro = 0.5, iteration =380, поскольку как правило на них (+-0.1) получались наилучшие результаты для популяции одного размера. Далее циклично подбиралось значение для популяции, чтобы достичь 10. К сожалению, необходимый для достижения луших значений постоянно рос: чтобы перейти от значения 7.2 к 8.2 потребовалось всего лишь увеличить популяцию на 2200 единиц, к 9.2 уже 28000, а значение 250000 все так же "застревало" на уровне 9.8 - 9.9 и занимало значительное количество времени 

---
The first stage of the work was an attempt to achieve the desired result - 10 - based on the existing code, only by selecting the parameters. At first, there was an attempt to determine by means of enumeration which parameters influence the result the most. The number of the population had the strongest effect; the larger it was, the larger the result became relative to launches with the same values ​​for mutation and crossover. Therefore, it was decided to take the values ​​mut_pro = 0.6, cross_pro = 0.5, iteration = 380, since, as a rule, they (+ -0.1) obtained the best results for a population of the same size. Then the value for the population was selected cyclically to reach 10. Unfortunately, the value needed to achieve the best values ​​was constantly growing: to go from 7.2 to 8.2, it was only necessary to increase the population by 2200 units, to 9.2 already 28000, and the value of 250,000 is still the same " got stuck "at 9.8 - 9.9 and took a significant amount of time

___
Plot for this attempt: 

![plot](https://cdn.discordapp.com/attachments/389543693561888778/788082725353553940/Screenshot_from_2020-12-14_18-33-51.png)

##2

Вторым этапом было все-таки найти такие функции мутации и кроссовера, чтобы достичь нужного результата за адекватное время. В качестве мутации происходил свап значений с 50% вероятностью, кроссовер высчитывал новые значения для индивидумов с некоторой случайной вероятностью. В результате был получен необходимый результат за малое время выполнения.

___

The second stage was to find such mutation and crossover functions in order to achieve the desired result in an adequate time. As a mutation, the values were swapped with a 50% probability, the crossover calculated new values for individuals with a certain random probability. As a result, the required result was obtained in a short execution time.

![plot](https://cdn.discordapp.com/attachments/389543693561888778/788082438127091752/Screenshot_from_2020-12-14_19-26-14.png)
